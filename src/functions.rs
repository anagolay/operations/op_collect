use an_operation_support::describe;

#[cfg(not(feature = "std"))]
use alloc::{format, string::String};

/// op_collect
///
/// Depending on the cardinality of the inputs, either return the element passed in input as identity
/// if only one element is passed, or a vector containing all inputs if several.
///
/// The `describe` attribute allow generation of manifest
/// with the following explicit properties:
///
///  * _config_: no configuration parameters
///  * _features_: support std lib as well as nostd
///
/// # Arguments
///  - inputs: one or more occurrences of an expression
///  - configuration: expression (unused)
///
/// # Expansion
/// The macro will match either the single input or a multiplicity of them and expand accordingly in
/// a block so that the block result can be assigned.
#[describe([
    config = [],
    features = [
      "std"
    ],
])]
#[macro_export]
macro_rules! execute {
    ( $x:expr; $c:expr ) => {
       {
            #[cfg(not(feature = "std"))]
            extern crate alloc;
            #[cfg(not(feature = "std"))]
            let _config: alloc::collections::btree_map::BTreeMap<String, String> = $c;

            #[cfg(feature = "std")]
            let _config: std::collections::BTreeMap<String, String> = $c;

            $x
        }
    };
    ( $( $x:expr ),* ; $c:expr ) => {
        {
            #[cfg(not(feature = "std"))]
            extern crate alloc;
            #[cfg(not(feature = "std"))]
            let _config: alloc::collections::btree_map::BTreeMap<String, String> = $c;

            #[cfg(feature = "std")]
            let _config: std::collections::BTreeMap<String, String> = $c;

            let inputs = vec![$( $x ),*];

            inputs
        }
    };
}

#[cfg(test)]
mod tests {
    use std::collections::BTreeMap;

    #[test]
    fn test_describe() {
        use an_operation_support::operation::OperationManifestData;
        use serde_json;

        let manifest_data: OperationManifestData =
            serde_json::from_str(&crate::describe()).unwrap();

        assert_eq!("op_collect", manifest_data.name);
        assert_eq!(0, manifest_data.inputs.len());
        assert_eq!("", manifest_data.output);
        assert_eq!("std", manifest_data.features.get(0).unwrap());
    }

    #[tokio::test]
    async fn test_execute() {
        let input = "hello".to_string();
        let op_output = execute!(&input; BTreeMap::new());

        assert_eq!(input, *op_output);

        let op_output = execute!(&input, &input; BTreeMap::new());

        assert_eq!(2, op_output.len());
        assert_eq!(input, **op_output.get(0).unwrap());
        assert_eq!(input, **op_output.get(1).unwrap());
    }
}
