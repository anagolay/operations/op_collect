#[cfg(not(feature = "std"))]
use alloc::{
    string::{String, ToString},
    vec::Vec,
};
use js_sys::{Array, Map};
#[cfg(debug_assertions)]
use std::panic;
use wasm_bindgen::prelude::wasm_bindgen;
use wasm_bindgen::JsValue;

/// op_collect
///
/// Depending on the cardinality of the inputs, either return the element passed in input as identity
/// if only one element is passed, or a vector containing all inputs if several.
/// This is the WASM binding of the Operation `execute()` function
///
/// # Arguments
///  * operation_inputs: collection of serialized inputs
///  * config: configuration map
///
/// # Return
/// A promise that resolves to the appropriate output if it succeeds, or to an error message
/// [`String`] otherwise
#[wasm_bindgen(js_name=execute)]
pub async fn wasm_execute(
    operation_inputs: Vec<JsValue>,
    _config: Map,
) -> Result<JsValue, JsValue> {
    #[cfg(debug_assertions)]
    panic::set_hook(Box::new(console_error_panic_hook::hook));

    if operation_inputs.len() == 1 {
        Ok(operation_inputs.get(0).unwrap().clone())
    } else {
        let array = Array::new();
        operation_inputs.iter().for_each(|input| {
            array.push(input);
        });
        Ok(array.into())
    }
}

/// Output manifest
#[wasm_bindgen(js_name=describe)]
pub fn wasm_describe() -> String {
    #[cfg(debug_assertions)]
    panic::set_hook(Box::new(console_error_panic_hook::hook));
    crate::describe()
}
