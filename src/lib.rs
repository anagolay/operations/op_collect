#![cfg_attr(not(feature = "std"), no_std)]

#[cfg(not(feature = "std"))]
extern crate alloc;

// Export Functions
mod functions;
pub use functions::*;

#[cfg(all(feature = "js", target_arch = "wasm32"))]
mod js;
#[cfg(all(feature = "js", target_arch = "wasm32"))]
pub use js::*;
