# op_collect

`op_collect` is an Anagolay Operation that produces as single output the single input received, or outputs the collection of the inputs if several.  

This Operation inputs and output are empty in the manifest since their type and number is not statically defined.

```
{
  "name": "op_collect",
  "description": "Anagolay collect Operation. Depending on the cardinality of the inputs, either return the element passed in input as identity if only one element is passed, or a vector containing all inputs if several.",
  "inputs": [],
  "config": {},
  "groups": [
    "FLOWCONTROL"
  ],
  "output": "",
  "repository": "https://gitlab.com/anagolay/operations/op_collect",
  "license": "Apache 2.0",
  "features": [
    "std"
  ]
}
```

## Installation

In the Cargo.toml file of your Workflow, specify a dependency toward **op_collect** crate:

```toml
[dependencies]
op_collect = { git = "https://gitlab.com/anagolay/operation/op_collect" }
```

## Features

* **anagolay** Used to build this crate as dependency for other operations (default)
* **js** Used to build the javascript bindings (default)
* **std** Enable preferential compilation for std environment (default) 
* **debug_assertions** Used to produce helpful debug messages (default), requires **std**
* **test** Used to build and execute tests, requires **std**

## Usage

### Execution

From Rust:

```rust
use op_collect;

let input = "hello".to_string();
let output = execute!(&input; BTreeMap::new());

assert_eq!(input, *op_output);

let output = execute!(&input, &input; BTreeMap::new());

assert_eq!(2, output.len());
assert_eq!(input, **output.get(0).unwrap());
assert_eq!(input, **output.get(1).unwrap());
```
Or in WASM environment:

```javascript
import { execute } from "@anagolay/op_collect"

async function main() {
  const input = "example"
  const config = {}
  let output = await execute([input], config)
  assert(input === output)

  let output = await execute([input, input], config)
  assert(input === output[0])
  assert(input === output[1])
}
```

## Development

It is suggested to use the Vscode `.devcontainer` or any other sandboxed environment like Gitpod because the Operations must be developed under the same environment as they are published.

Git hooks will be initialized on first build or a test. If you wish to init them before run this in your terminal:

```shell
rusty-hook init
```

To help you with writing consistent code we provide you with following:

-   `.gitlab-ci.yml` for your testing and fmt checks
-   `rusty-hook.toml` for the `pre-push` fmt check so your CI is green.

### Manifest generation

```shell
makers manifest
```

## License

[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)
